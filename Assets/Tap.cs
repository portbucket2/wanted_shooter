﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Tap : MonoBehaviour
{
    public Text number;
    public int numVal;
    // Start is called before the first frame update
    void Start()
    {
        number.text = "" + numVal;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            numVal += 5;
            number.text = "" + numVal;
        }
    }
}
